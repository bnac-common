import java.util.*;
import java.io.*;
import javax.xml.parsers.*;
import cl.uchile.dcc.bnac.*;

public class Test
{
    static public void main (String[] args) throws Exception
    {
        if (args[0].equals("meml")) {
            memlTest(args[1]);
        } else if (args[0].equals("info")) {
            infoTest(args[1]);
        }
    }

    static public void memlTest (String path) throws Exception
    {
        MuseumEnvironment me;
        me = MuseumEnvironment.fromFile(new File(path));
        if (me == null) { System.out.println("failed"); }
        else {
            System.out.println(me.get("Title"));
            CObject[] objs = me.objectArray();
            Capture[] caps = me.captureArray();
            Hall[] hals = me.hallArray();

            System.out.println("[Objects]");
            for (CObject obj: objs) {
                System.out.printf("    %s:\n", obj.get("Id"));
                System.out.printf("        width: %s\n", obj.get("Width"));
                System.out.printf("        height: %s\n", obj.get("Height"));
            }
            System.out.println("[Captures]");
            for (Capture cap: caps) {
                System.out.printf("    %s -> %s:\n", cap.get("Id"),
                        cap.getReferencedObject().get("Id"));
                System.out.printf("        src: %s\n", cap.get("Resource"));
                System.out.printf("        type: %s\n", cap.get("Type"));
                System.out.printf("        date: %s\n", cap.get("Date"));
            }
            System.out.println("[Halls]");
            for(Hall hal:hals){
                System.out.printf("    %s:\n", hal.get("Id"));
                int i=0;
                for (Wall wal: hal.wallArray()) {
                    System.out.printf("        wall %d:\n", i++);
                    for (PlacedCapture pc: wal.placedCaptureArray()) {
                        System.out.printf("            %s: %s\n",
                                pc.id, pc.capture.get(Attribute.Id));
                    }
                    for (Door d: wal.doorArray()) {
                        System.out.printf(
                                "            %s +%s @%s\n",
                                d.getId(),
                                d.getDimensions().toString(),
                                d.getPosition().toString());
                    }
                }
                for (Hall.StartingPoint sp: hal.startingPointArray()) {
                    System.out.printf("        %s %s %.2f˚\n",
                            sp.id,
                            sp.pos.toString(),
                            sp.angle);
                }
            }

            System.out.println("[Links]");
            for (Link l: me.linkArray()) {
                System.out.printf("    %s:(%s<->%s)%n",
                        l.getId(), l.d1, l.d2);
            }

            System.out.println("[Routes]");
            for (Route r: me.routeArray()) {
                System.out.printf("    %s%n", r.getId());
                for (String s: r.captureArray()) {
                    System.out.printf("        %s%n", s);
                }
            }
        }

        MemlWriter.write(me, "/tmp/memltest.xml");
    }

    static public void infoTest (String path) throws Exception
    {
        InfoSet is = new InfoSet();
        File file;
        file = new File(path);
        if (!file.exists()) { System.out.println("failed"); }

        InfoParser ip = new InfoParser(is);
        SAXParserFactory.newInstance().newSAXParser().parse(file, ip);

        for (InfoSet.InfoPage page: is.pageArray()) {
            System.out.printf("page %s:%n", page.getId());
            if (page.getNext() != null) {
                System.out.printf("  next=%s%n", page.getNext());
            }
            if (page.getImage() != null) {
                System.out.printf("  image=%s%n", page.getImage());
            }
            for (Map.Entry<String, InfoSet.InfoEntry> entry:
                    page.entrySet()) {
                System.out.printf("  entry \"%s\":%n", entry.getValue().lang);
                System.out.printf("    title=%s%n", entry.getValue().title);
                System.out.printf("    descr%n%s%n", entry.getValue().descr);
            }
        }

        InfoWriter.write(is, "/tmp/infotest.xml");
    }
}

