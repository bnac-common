/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.util.ArrayList;
import java.util.Properties;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;

public class Hall
{
    protected float height;
    protected ArrayList<Point2f> corners;
    protected ArrayList<Wall> walls;
    protected ArrayList<StartingPoint> sps;
    protected Point2f min;
    protected Point2f max;
    protected Properties props;

    public Hall (String id)
    {
        BnacLog.trace("new Hall %s", id);
        props = new Properties();
        props.setProperty(Attribute.Id.name(), id);
        props.setProperty("name", id);

        this.height = 3.0f;
        this.corners = new ArrayList<Point2f>();
        walls = new ArrayList<Wall>();
        sps = new ArrayList<StartingPoint>();

        Point2f[] cs = {
            new Point2f(0.0f, 0.0f),
            new Point2f(0.0f, 10.0f),
            new Point2f(10.0f, 10.0f),
            new Point2f(10.0f, 0.0f)
        };
        setCorners(cs);
    }

    public Hall (String id, float height, Point2f[] corners)
    {
        if (corners.length > 0) {
            BnacLog.trace("new Hall %s %.02f >>", id, height);
            for (Point2f corner: corners) {
                BnacLog.trace("\t(%.02f,%.02f)", corner.x, corner.y);
            }
        } else {
            BnacLog.trace("new Hall %s %.02f >><<", id, height);
        }
        BnacLog.trace("<<");
        props = new Properties();
        props.setProperty(Attribute.Id.name(), id);

        this.height = height;
        this.corners = new ArrayList<Point2f>();
        walls = new ArrayList<Wall>();
        sps = new ArrayList<StartingPoint>();

        setCorners(corners);
    }

    public void setHeight (float h)
    {
        BnacLog.debug("set height %.02f", h);
        height = h;
    }

    public float getHeight () { return height; }

    public void setCorners (float[] ps)
    {
        Point2f[] r = new Point2f[ps.length/2];
        for (int i=0; i<ps.length; i+=2) {
            r[i/2] = new Point2f(ps[i], ps[i+1]);
        }
        setCorners(r);
    }
    public void setCorners (Point2f[] ps)
    {
        if (!Util.isClockwise(ps)) { return; }

        corners.clear();
        for (Point2f p: ps) { corners.add(p); }

        for (int i=0; i<ps.length; ++i) {
            walls.add(new Wall(ps[i], ps[(i+1)%ps.length]));
        }

        updateMaxMin();
    }
    public Wall addCorner (int idx, Point2f p)
    {
        BnacLog.debug("addCorner %d (%.02f,%.02f)", idx, p.x, p.y);
        Wall wal = walls.get((idx>0)?idx-1:walls.size()-1);
        Wall newwall = wal.split(p);

        corners.add(idx, p);
        walls.add(idx, newwall);

        min = max = null;
        for (Point2f corner: corners) {
            BnacLog.trace("(%.02f,%.02f)", corner.x, corner.y);
        }
        for (int i=0; i<walls.size(); ++i) {
            BnacLog.trace("%d (%.02f,%.02f)-(%.02f,%.02f)", i,
                    walls.get(i).origin2f().x, walls.get(i).origin2f().y,
                    walls.get(i).end2f().x, walls.get(i).end2f().y);
        }
        return newwall;
    }
    public void moveCorner (int idx, Point2f p)
    {
        BnacLog.debug("moveCorner %d (%.02f,%.02f)->(%.02f,%.02f)",
                idx, corners.get(idx).x, corners.get(idx).y, p.x, p.y);
        int prv = (idx>0) ? idx-1 : corners.size()-1;

        corners.get(idx).set(p);
        walls.get(prv).setEnd(p);
        walls.get(idx).setOrigin(p);

        min = max = null;
    }
    public Wall removeCorner (int idx)
    {
        BnacLog.debug("removeCorner %d", idx);
        if (corners.size() > 3) {
            Wall prev = walls.get((idx>0)?idx-1:walls.size()-1);
            Wall wall = walls.get(idx);
            Wall ret = new Wall(prev.origin2f(), wall.end2f());
           
            corners.remove(idx);
            if (idx > 0) {
                walls.remove((idx>0)?idx-1:walls.size()-1);
                walls.remove((idx>0)?idx-1:walls.size()-1);
                walls.add(idx-1, ret);
            } else {
                walls.remove(walls.size()-1);
                walls.remove(0);
                walls.add(walls.size(), ret);
            }

            for (Point2f corner: corners) {
                BnacLog.trace("(%.02f,%.02f)", corner.x, corner.y);
            }
            for (int i=0; i<walls.size(); ++i) {
                BnacLog.trace("%d (%.02f,%.02f)-(%.02f,%.02f)", i,
                        walls.get(i).origin2f().x, walls.get(i).origin2f().y,
                        walls.get(i).end2f().x, walls.get(i).end2f().y);
            }

            return ret;
        }
        return null;
    }
    public Point2f getCorner (int idx)
    {
        return new Point2f(corners.get(idx));
    }
    public Point2f[] cornerArray () {
        return corners.toArray(new Point2f[0]);
    }
    public int cornerCount () { return corners.size(); }

    protected void updateMaxMin ()
    {
        min = new Point2f(Float.MAX_VALUE, Float.MAX_VALUE);
        max = new Point2f(Float.MIN_VALUE, Float.MIN_VALUE);
        for (Point2f p: corners) {
            if (p.x < min.x) { min.x = p.x; }
            else if (p.x > max.x) { max.x = p.x; }
            if (p.y < min.y) { min.y = p.y; }
            else if (p.y > max.y) { max.y = p.y; }
        }
    }

    public Wall setWall (int idx, Wall w)
    {
        BnacLog.debug("setWall %d (%.02f,%.02f)-(%.02f,%.02f)", idx,
                w.origin2f().x, w.origin2f().y,
                w.end2f().x, w.end2f().y);
        min = max = null;
        return walls.set(idx, w);
    }
    public Wall[] wallArray () { return walls.toArray(new Wall[0]); }
    public Wall getWall (int i) { return walls.get(i); }
    public int wallCount () { return walls.size(); }

    public int getParentWall (String id)
    {
        for (int i=0; i<walls.size(); ++i) {
            if (walls.get(i).getDoor(id) != null ||
                    walls.get(i).getPlacedCapture(id) != null) {
                return i;
            }
        }
        return -1;
    }

    public PlacedCapture getPlacedCapture (String id)
    {
        return walls.get(getParentWall(id)).getPlacedCapture(id);
    }

    public Door getDoor (String id)
    {
        for (Wall wall: walls) {
            if (wall.getDoor(id) != null) {
                return wall.getDoor(id);
            }
        }
        return null;
    }

    public Point2f dif ()
    {
        Point2f min = min();
        Point2f max = max();
        return new Point2f(max.x-min.x, max.y-min.y);
    }
    public Point2f min ()
    {
        if (min == null || max == null) { updateMaxMin(); }
        return new Point2f(min);
    }
    public Point2f max ()
    {
        if (min == null || max == null) { updateMaxMin(); }
        return new Point2f(max);
    }
    public Point2f centre2f (boolean negY)
    {
        return new Point2f(dif().x/2, dif().y/2 * (negY ? -1 : 1));
    }
    public Point3f centre3f ()
    {
        return new Point3f(dif().x/2, 0.0f, -dif().y/2);
    }

    public void set (Attribute prop, String val)
    {
        props.setProperty(prop.name(), val);
    }
    public String get (Attribute prop)
    {
        return props.getProperty(prop.name());
    }

    public void set (String prop, String val)
    {
        props.setProperty(prop, val);
    }
    public String get (String prop)
    {
        return props.getProperty(prop);
    }

    public void setId (String id)
    {
        props.setProperty(Attribute.Id.name(), id);
    }
    public String getId () { return props.getProperty(Attribute.Id.name()); }

    public String getName () { return props.getProperty("name"); }
    
    public void setName (String name) { props.setProperty("name", name); }

    public void addStartingPoint (String id, Point2f pos, float angle)
    {
        sps.add(new StartingPoint(id, pos, angle));
    }
    public StartingPoint getStartingPoint (String id)
    {
        for (StartingPoint sp: sps)
        {
            if (sp.getId().equals(id)) { return sp; }
        }
        return null;
    }
    public StartingPoint getFirstStartingPoint () { return sps.get(0); }
    public StartingPoint[] startingPointArray ()
    {
        return sps.toArray(new StartingPoint[0]);
    }

    public class StartingPoint
    {
        public String id;
        public Point2f pos;
        public float angle;

        public StartingPoint (String id, Point2f pos, float angle)
        {
            this.id = id;
            this.pos = new Point2f(pos);
            this.angle = angle;
        }

        public String getId () { return id; }
        public Point3f getPosition3f ()
        {
            return new Point3f(pos.x, 1.70f, -pos.y);
        }
    }
}

