/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class InfoSet
{
    protected ArrayList<InfoPage> pages;

    public InfoSet ()
    {
        pages = new ArrayList<InfoPage>();
    }

    public InfoPage getPage (String id)
    {
        for (InfoPage ip: pages) {
            if (ip.getId().equals(id)) { return ip; }
        }
        return null;
    }

    public InfoPage makePage (String id)
    {
        InfoPage page = new InfoPage(id);
        pages.add(page);
        BnacLog.debug("new infopage: %s", id);
        return page;
    }

    public InfoPage removePage (String id)
    {
        for (int i=0; i<pages.size(); ++i) {
            if (pages.get(i).getId().equals(id)) {
                return pages.remove(i);
            }
        }
        return null;
    }

    public InfoPage[] pageArray () { return pages.toArray(new InfoPage[0]); }

    public boolean hasPage (String id)
    {
        for (InfoPage ip: pages) {
            if (ip.getId().equals(id)) { return true; }
        }
        return false;
    }

    static public InfoSet fromFile (File file)
        throws IOException, SAXException, ParserConfigurationException
    {
        InfoSet info = new InfoSet();
        InfoParser ip = new InfoParser(info);
        SAXParserFactory spfactory = SAXParserFactory.newInstance();
        SAXParser parser = spfactory.newSAXParser();
        SAXParserFactory.newInstance().newSAXParser().parse(file, ip);

        return info;
    }

    public class InfoPage
    {
        protected String id;
        protected String img;
        protected Hashtable<String, InfoEntry> entries;
        protected String next;

        public InfoPage (String id)
        {
            this.id = id;
            img = null;
            entries = new Hashtable<String, InfoEntry>();
            next = null;
        }

        public void setId (String id) { this.id = id; }

        public void setImage (String path) { img = path; }
        public InfoEntry makeEntry (String lang)
        {
            InfoEntry entry = new InfoEntry(lang);
            entries.put(lang, entry);
            BnacLog.debug("new entry in %s: %s", id, lang);
            return entry;
        }
        public void setNext (String id) { next = id; }

        public String getId () { return id; }
        public String getImage () { return img; }
        public InfoEntry getEntry (String lang) { return entries.get(lang); }
        public String getNext () { return next; }

        public Set<Map.Entry<String, InfoEntry>> entrySet ()
        {
            return entries.entrySet();
        }
    }

    public class InfoEntry
    {
        public String lang;
        public String title;
        public String descr;

        public InfoEntry (String lang) { this(lang, null, null); }

        public InfoEntry (String lang, String title, String description)
        {
            this.lang = lang;
            this.title = title;
            this.descr = description;
        }
    }
}

