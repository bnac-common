/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;

public class PlacedCapture
{
    public String id;
    public Capture capture;
    public Point2f t;
    public float angle;
    public float scale;

    public PlacedCapture (String id, Capture cap)
    {
        this(id, cap, new Point2f(0.0f, 1.7f), 0.0f, 1.0f);
    }

    public PlacedCapture (String id, Capture cap, Point2f tr,
            float a, float s)
    {
        this.id = id;
        capture = cap;
        t = new Point2f(tr);
        angle = a;
        scale = s;
    }

    public String getId () { return id; }

    public Point3f translation3f () { return new Point3f(t.x, t.y, 0.0f); }

    public boolean equals (Object o)
    {
        if (o.getClass().getName().equals(this.getClass().getName())) {
            PlacedCapture pc = (PlacedCapture) o;
            if (capture.equals(pc.capture) && t.equals(pc.t) &&
                    Math.abs(angle - pc.angle) < 0.001f &&
                    Math.abs(scale - pc.scale) < 0.001f) {
                return true;
            }
        }
        return false;
    }
}

