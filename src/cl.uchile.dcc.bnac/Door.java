/*
 *  bnac : virtual museum environment creation/manipulation project
 *  Copyright (C) 2010 Felipe Cañas Sabat
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package cl.uchile.dcc.bnac;

import javax.vecmath.Point2f;
import javax.vecmath.Point3f;

public class Door
{
    protected String id;
    protected Point2f dims;
    protected Point2f pos;
    /** Angle, in radians. */
    protected float angle;
    protected float scale;

    public Door (String id)
    {
        this(id, new Point2f(1.4f, 2.5f), new Point2f(0.7f, 1.25f),
                0.0f, 1.0f);
    }

    public Door (String id, Point2f dims, Point2f pos,
            float a, float s)
    {
        BnacLog.trace("new Door %s, %.02fx%.02f," +
                " @(%.02f,%.02f), ang %.02f, sca %.02f",
                id, dims.x, dims.y, pos.x, pos.y, Math.toDegrees(a), s);
        this.id = id;
        this.dims = new Point2f(dims);
        this.pos = new Point2f(pos);
        angle = a;
        scale = s;
    }

    public void setDimensions (float w, float h) { dims.set(w, h); }
    public void setPosition (float x, float y) { pos.set(x, y); }
    public Point2f getDimensions () { return dims; }
    public Point2f getPosition () { return pos; }
    public Point3f getPosition3f ()
    {
        return new Point3f(pos.x, pos.y, 0.0f);
    }
    public void setAngle (float angle) { this.angle = angle; }
    public float getAngle () { return angle; }
    public void setScale (float scale) { this.scale = scale; }
    public float getScale () { return scale; }

    public String getId () { return id; }
}

